import React, { Component } from 'react';
import {Link} from 'react-router-dom'
import '../css/products.css';

const Product = (props) => {

    const {imagen, nombre, precio, id} = props.info;

    return(
        <li>
            <img src={`img/${imagen}.png`} alt={"Nombre"} />
            <p>{nombre} <span>$ {precio}</span> </p>
            <Link to={`/product/${id}`}>Mas Informacion</Link>
        </li>
    )
}

export default Product;