import React, { Component } from 'react';
import '../css/about.css';

const About = () => {
    return(
        <div className={"contenedor-nosotros"}>
            <div className={"imagen"}>
                <img src={"/img/camisa_1.png"} alt={"Imagen Nosotros"} />
            </div>
            <div className={"contenido"}>
                <h2>Sobre Nosotros</h2>
                <p>
                    Contenido de texto
                </p>
            </div>
        </div>
    )
};

export default About;
