import React, { Component } from 'react';
import '../css/singleProduct.css'

const SingleProduct = (props) => {

    if(!props){
        return null;
    }

    const {imagen, nombre ,precio, descripcion} = props.product;

    return(
        <div className={"info-producto"}>
            <div className={"imagen"}>
                <img src={`/img/${imagen}.png`} alt={"Nombre"} />
            </div>
            <div className={"info"}>
                <h2>{nombre}</h2>
                <p className={"precio"}>$ {precio}</p>
                <p>{descripcion}</p>
            </div>
        </div>
    )
}

export default SingleProduct;