import React, { Component } from 'react';
import axios from 'axios';
import Product from "./Product";
import SearchBar from "./SearchBar";


class Products extends Component{

    state = {
        products : [],
        termSearched : ''
    };

    componentWillMount(){
        this.queryApi();
    }

    queryApi = () => {
        const {getAccessToken} = this.props.auth;
        const headers = {'Authorization' : `Bearer ${getAccessToken()}`}
        const url = 'http://localhost:5000/products';
        axios.get(url, {headers}).then(resp => this.setState(() => {
            return {products : resp.data};
        }))
    };

    login = () => {
        this.props.auth.login();
    };

    searched = (searchedText) => {

        let products = [...this.state.products];
        let result;

        result = products.filter(product => (
            product.nombre.toLowerCase().indexOf(searchedText.toLowerCase()) !== -1
        ));

        if(searchedText.length > 3){
            this.setState(() => {
                return {termSearched : searchedText, products: result};
            });
        }else{
            this.setState(() => {
                return {termSearched : ''};
            }, () => {
                this.queryApi();
            });
        }
    };

    render(){

        const {isAuthenticated} = this.props.auth;

        return(
            <div className={"productos"}>
                {
                    isAuthenticated() && (
                        <React.Fragment>
                            <h2>Nuestros Productos</h2>
                            <SearchBar searched = {this.searched} />

                            <ul className={"lista-productos"}>
                                {Object.keys(this.state.products).map(product => (
                                <Product
                                    info = {this.state.products[product]}
                                    key = {product}
                                />
                                ))}
                            </ul>
                        </React.Fragment>
                    )
                }
                {
                    !isAuthenticated() && (
                        <div className={"contenedor-boton"}>
                            <p>Para ver el contenido debes estar logueado </p>
                            <a className={"boton"} onClick={this.login}>Iniciar Sesion</a>
                        </div>
                    )
                }

            </div>

        )
    }
}

export default Products;