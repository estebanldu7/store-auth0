import React, { Component } from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import infoProducts from '../data/data.json';


class Router extends Component{







    //Component is for static route and render is for dynamic page.
    render(){

        let products = [...this.state.products];
        let termSearched = this.state.termSearched;
        let result;

        if(termSearched !== ''){
            result = products.filter(product => (
                product.nombre.toLowerCase().indexOf(termSearched.toLowerCase()) !== -1
            ))
        }else{
            result = products
        }

        return(
            <BrowserRouter>
                <Switch>
                    <div className="contenedor">


                        <Route exact path={"/"} render={() => (
                            <Products
                                products={result}
                                searched = {this.searched}
                            />
                        )}
                        />




                    </div>
                </Switch>
            </BrowserRouter>
        )
    }
}

export default Router;