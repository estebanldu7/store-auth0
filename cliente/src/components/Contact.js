import React from 'react';
import {Link} from 'react-router-dom';
import '../css/contact.css';

class Contact extends React.Component{
    render(){

        const {isAuthenticated} = this.props.auth;

        return(
            <React.Fragment>
                {
                    isAuthenticated() && (
                    <form>
                        <legend>Formulario de contacto</legend>

                        <div className={"input-field"}>
                            <label>Nombre</label>
                            <input type={"text"} placeholder={"Escribe tu nombre"}/>
                        </div>
                        <div className={"input-field"}>
                            <label>Email</label>
                            <input type={"emain"} placeholder={"Escribe tu email"}/>
                        </div>
                        <div className={"input-field"}>
                            <label>Mensaje</label>
                            <textarea></textarea>
                        </div>
                        <div className={"input-field enviar"}>
                            <input type={"submit"} value={"Enviar"}/>
                        </div>
                    </form>
                )}
                {
                    !isAuthenticated() && (
                        <div className={"contenedor-boton"}>
                            <p>Para ver el contenido debes estar logueado </p>
                            <a className={"boton"} onClick={this.login}>Iniciar Sesion</a>
                        </div>

                )}
            </React.Fragment>
        )
    }
}

export default Contact;