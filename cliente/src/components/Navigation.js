import React from 'react';
import {NavLink} from 'react-router-dom';
import '../css/navigation.css';

class Navigation extends React.Component{

    logout = () => {
        this.props.auth.logout();
    };

    login = () => {
        this.props.auth.login();
    };

    render(){

        const {isAuthenticated} = this.props.auth;
        let result;

        if(isAuthenticated()){
            result =  <a onClick={this.logout}>Cerrar Sesion</a>
        }else{
            result =  <a onClick={this.login}>Iniciar Sesion</a>
        }

        return(
            <nav className={"navegacion"}>
                <NavLink to={"/about"} activeClassName={"activo"}>Nosotros</NavLink>
                <NavLink to={"/products"} activeClassName={"activo"}>Productos</NavLink>
                <NavLink to={"/contact"} activeClassName={"activo"}>Contacto</NavLink>
                {result}
            </nav>
        )
    }
}

export default Navigation;