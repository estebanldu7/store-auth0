import React from 'react';
import { Route, Router } from 'react-router-dom';

//Auth0 Components
import Callback from './Callback/Callback';
import Auth from '../Auth/Auth';
import history from '../history';

//Owner Components
import SingleProduct from './SingleProduct'
import Navigatiopn from './Navigation';
import Products from './Products';
import Header from './Header';
import About from './About';
import Contact from './Contact';

const auth = new Auth();

const handleAuthentication = ({location}) => {
  if (/access_token|id_token|error/.test(location.hash)) {
    auth.handleAuthentication();
  }
}

export const makeMainRoutes = () => {
  return (
      <Router history={history}>
          <div className="contenedor">
              <Header/>
              <Navigatiopn auth={auth}/>
              <Route exact path={"/"} render={(props) => (
                  <Products
                      auth={auth} {...props}
                  />
                  )}
              />
              <Route exact path={"/about"} component={About} />
              <Route exact path={"/contact"} render={(props) => (
                  <Contact  auth={auth} {...props}/>
              )} />
              <Route exact path={"/products"} render={(props) => (
                  <Products
                      // searched = {this.searched}
                      auth={auth} {...props}
                  />
              )}
              />
              <Route exact path={"/product/:productId"} render={(props) => {
                  let idProduct = props.location.pathname.replace('/product/', '');
                  return(
                      <SingleProduct
                          product = {this.state.products[idProduct]}
                          auth={auth} {...props}
                      />
                  )
              }} />
              <Route path="/callback" render={(props) => {
                handleAuthentication(props);
                return <Callback {...props} />
              }}/>
        </div>
      </Router>
  );
}
